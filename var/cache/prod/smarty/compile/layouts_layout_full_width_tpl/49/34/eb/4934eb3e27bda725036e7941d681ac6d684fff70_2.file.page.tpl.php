<?php
/* Smarty version 3.1.33, created on 2019-04-21 09:04:37
  from '/home/boutique/src/boutique/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cbc6a65cfa792_96522752',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4934eb3e27bda725036e7941d681ac6d684fff70' => 
    array (
      0 => '/home/boutique/src/boutique/themes/classic/templates/page.tpl',
      1 => 1555804271,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cbc6a65cfa792_96522752 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8592124265cbc6a65cf6c60_44655538', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_21290423035cbc6a65cf7631_94387668 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_5052852595cbc6a65cf70e9_70969127 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21290423035cbc6a65cf7631_94387668', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_14407234515cbc6a65cf8b65_46782872 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_6587217985cbc6a65cf9148_52723499 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_11212361995cbc6a65cf8607_40133118 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14407234515cbc6a65cf8b65_46782872', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6587217985cbc6a65cf9148_52723499', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_20319479485cbc6a65cf9d83_26521972 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_9116171275cbc6a65cf99d2_25912130 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20319479485cbc6a65cf9d83_26521972', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_8592124265cbc6a65cf6c60_44655538 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_8592124265cbc6a65cf6c60_44655538',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_5052852595cbc6a65cf70e9_70969127',
  ),
  'page_title' => 
  array (
    0 => 'Block_21290423035cbc6a65cf7631_94387668',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_11212361995cbc6a65cf8607_40133118',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_14407234515cbc6a65cf8b65_46782872',
  ),
  'page_content' => 
  array (
    0 => 'Block_6587217985cbc6a65cf9148_52723499',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_9116171275cbc6a65cf99d2_25912130',
  ),
  'page_footer' => 
  array (
    0 => 'Block_20319479485cbc6a65cf9d83_26521972',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5052852595cbc6a65cf70e9_70969127', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11212361995cbc6a65cf8607_40133118', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9116171275cbc6a65cf99d2_25912130', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
